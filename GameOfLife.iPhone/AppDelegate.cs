﻿using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace GameOfLife.IPhone
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;
		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			window = new UIWindow (UIScreen.MainScreen.Bounds);

			var stringsRepository = new SimpleStringsRepository ();
			stringsRepository.Save ("GliderStateKey", "0,4\n0,5\n0,6\n1,6\n2,5\n3,0\n3,8\n3,9\n4,0\n4,2\n4,7\n4,9\n5,0\n5,1\n5,9\n7,5\n8,4\n9,4\n9,5\n9,6\n");

			window.RootViewController = new UINavigationController(new GameViewController (stringsRepository));
			window.MakeKeyAndVisible ();
			
			return true;
		}


	}
}

