﻿using System;
using GameOfLife.Core;
using MonoTouch.Foundation;

namespace GameOfLife.IPhone
{
	public class SimpleStringsRepository : IStringsRepository
	{
		private NSUserDefaults _userDefaults;

		public SimpleStringsRepository ()
		{
			_userDefaults = NSUserDefaults.StandardUserDefaults;
		}

		#region IStringsRepository implementation

		public string GetByKey (string key)
		{
			if (string.IsNullOrEmpty(key)) {
				throw new ArgumentNullException ("key");
			}

			return _userDefaults.StringForKey (key);
		}

		public void Save (string key, string value)
		{
			if (string.IsNullOrEmpty(key)) {
				throw new ArgumentNullException ("key");
			}

			if (string.IsNullOrEmpty(value)) {
				throw new ArgumentNullException ("value");
			}

			_userDefaults.SetString (value, key);
			_userDefaults.Synchronize ();
		}

		#endregion
	}
}

