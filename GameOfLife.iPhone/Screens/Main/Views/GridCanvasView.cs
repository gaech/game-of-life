﻿using System;
using System.Drawing;
using GameOfLife.Core.Domain;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;

namespace GameOfLife.IPhone
{
	public class GridCanvasView : UIView
	{
		private const int CanvasMultiplier = 2;

		private readonly Grid _grid;
		private readonly int _cellSize;

		private UInt64 _originalTopX;
		private UInt64 _originalTopY;
		private UInt64 _topX;
		private UInt64 _topY;
		private UInt64 _visibleColls;
		private UInt64 _visibleRows;

		private PointF _offset;

		public GridCanvasView (RectangleF parentBounds, Grid grid, int cellSize)
		{
			_grid = grid;
			_cellSize = cellSize;

			_topX = grid.Size / 2;
			_topY = grid.Size / 2;

			int colls = CanvasMultiplier*(int)parentBounds.Width / cellSize;
			colls = colls % 2 == 0 ? colls : colls + 1;
			_visibleColls = Convert.ToUInt64 (colls);
			_topY -= _visibleColls / 2;

			int rows = CanvasMultiplier*(int)parentBounds.Height / cellSize;
			rows = rows % 2 == 0 ? rows : rows + 1;
			_visibleRows = Convert.ToUInt64 (rows);
			_topX -= _visibleRows / 2;

			_originalTopX = _topX;
			_originalTopY = _topY;

			Frame = new RectangleF(PointF.Empty, new SizeF(colls*_cellSize, rows*_cellSize));

			UITapGestureRecognizer tap = new UITapGestureRecognizer((UITapGestureRecognizer rec) => {
				var point = rec.LocationInView(this);

				int row = (int)(point.Y-_offset.Y)/_cellSize;
				int col = (int)(point.X-_offset.X)/_cellSize;

				_grid.ToggleCellState(_topX + Convert.ToUInt64(row), _topY + Convert.ToUInt64(col));
				SetNeedsDisplay();
			});

			AddGestureRecognizer(tap);
		}

		public void Reset()
		{
			_topX = _originalTopX;
			_topY = _originalTopY;

			_offset = PointF.Empty;
		}

		public void Move(float dX, float dY)
		{
			var offsetX = dX + _offset.X;
			int colls = -(int)offsetX / _cellSize;
			offsetX = offsetX + _cellSize*colls;

			if (colls < 0) {
				_topY -= Convert.ToUInt64 (Math.Abs (colls));
			} else {
				_topY += Convert.ToUInt64 (Math.Abs (colls));
			}
			

			var offsetY = dY  + _offset.Y;
			int rows = -(int)offsetY / _cellSize;
			offsetY = offsetY + _cellSize*rows;

			if (rows < 0) {
				_topX -= Convert.ToUInt64 (Math.Abs (rows));
			} else {
				_topX += Convert.ToUInt64 (Math.Abs (rows));
			}

			_offset = new PointF (offsetX, offsetY);

			SetNeedsDisplay ();
		}

		public override void Draw (RectangleF rect)
		{
			var context = UIGraphics.GetCurrentContext();
			context.SetRGBFillColor(1, 1, 1, 1);
			context.FillRect(Bounds);

			context.SetLineWidth(0.5f);
			context.SetRGBStrokeColor(0, 0, 0, 0.25f);


			// draw horizontal grid lines
			float y = _offset.Y;
			for (ulong i = 0; i < _visibleRows; i++) {
				context.MoveTo(0,y); //start at this point	

				context.AddLineToPoint(Frame.Width, y); //draw to this point

				context.StrokePath();

				y += _cellSize;
			}

			// draw vertical grid lines
			float x = _offset.X;
			for (ulong i = 0; i < _visibleColls; i++) {
				context.MoveTo(x,0); //start at this point	

				context.AddLineToPoint(x, Frame.Height); //draw to this point

				context.StrokePath();

				x += _cellSize;
			}


			context.SetRGBFillColor(1, 0, 0, 0.7f);
			context.SetRGBStrokeColor(1, 1, 1, 0.5f);

			UInt64 bottomX = _topX + _visibleRows;
			UInt64 bottomY = _topY + _visibleColls;

			foreach (var coordinate in _grid.AliveCellsCoordinates) {
				if (_topX <= coordinate.X
				   && bottomX >= coordinate.X
					&& _topY <= coordinate.Y
				   && bottomY >= coordinate.Y) {


					int row = Convert.ToInt32(coordinate.X - _topX);
					int coll = Convert.ToInt32(coordinate.Y - _topY);;

					var rectangle = new RectangleF(_offset.X + _cellSize*coll, _offset.Y + _cellSize*row, _cellSize, _cellSize);

					context.FillRect(rectangle);
					context.StrokeRect(rectangle);
				}
			}
		}
	}
}

