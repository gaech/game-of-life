﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using GameOfLife.Core.Domain;

namespace GameOfLife.IPhone
{
	[Register("GridView")]
	public class GridView : UIScrollView
	{
		private PointF _startOffset;
		private GridCanvasView _canvas;

		private PointF _contentCenter;

		public GridView (IntPtr handle) : base(handle)
		{
		}

		public void Init (Grid grid, int cellSize)
		{
			Bounces = false;
			ShowsHorizontalScrollIndicator = false;
			ShowsVerticalScrollIndicator = false;

			WeakDelegate = this;

			_canvas = new GridCanvasView (Bounds, grid, cellSize);
			Add (_canvas);

			ContentSize = _canvas.Bounds.Size;
			_contentCenter = new PointF (ContentSize.Width / 2 - Bounds.Width / 2, ContentSize.Height / 2 - Bounds.Height / 2);
			ContentOffset = _contentCenter;
		}

		public void Reset()
		{
			_canvas.Reset ();
			ContentOffset = _contentCenter;

			Update ();
		}

		public void Update()
		{
			_canvas.SetNeedsDisplay ();
		}

		[Export("scrollViewWillBeginDecelerating:")]
		public void WillBeginDecelerating (UIScrollView scrollView)
		{
			// HACK: deceleration off
			scrollView.SetContentOffset (scrollView.ContentOffset, false);
		}

		[Export ("scrollViewWillBeginDragging:")]
		public void WillBeginDragging (UIScrollView scrollView)
		{
			_startOffset = scrollView.ContentOffset;
		}

		[Export ("scrollViewDidEndDragging:willDecelerate:")]
		public void DidEndDragging (UIScrollView scrollView, bool willDecelerate)
		{
			var offsetX = _startOffset.X - scrollView.ContentOffset.X ;

			var offsetY = _startOffset.Y - scrollView.ContentOffset.Y;

			scrollView.ContentOffset = _contentCenter;

			_canvas.Move (offsetX, offsetY);
		}
	}
}

