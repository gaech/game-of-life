// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace GameOfLife.IPhone
{
	[Register ("GameViewController")]
	partial class GameViewController
	{
		[Outlet]
		GameOfLife.IPhone.GridView _gridView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_gridView != null) {
				_gridView.Dispose ();
				_gridView = null;
			}
		}
	}
}
