﻿using System;
using System.Collections.Generic;
using GameOfLife.Core.Domain;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using GameOfLife.Core;

namespace GameOfLife.IPhone
{
	public partial class GameViewController : UIViewController
	{
		private const double GridViewUpdateInterval = 0.25;
		private const int CellSize = 20; // pixels
		private const string DefaultStateKey = "GridStateKey";
		private const string GliderStateKey = "GliderStateKey";

		private readonly Grid _grid;
		private NSTimer _timer;

		private CellCoordinatesSerializer _serializer;
		private IStringsRepository _stringsRepository;

		private UIBarButtonItem _playButton;
		private UIBarButtonItem _pauseButton;

		private UIBarButtonItem[] _playStateButtons;
		private UIBarButtonItem[] _pauseStateButtons;

		private UIActionSheet _actionSheet;

		public GameViewController (IStringsRepository stringsRepository) : base ("GameViewController", null)
		{
			_stringsRepository = stringsRepository;
			_grid = new Grid ();
			_serializer = new CellCoordinatesSerializer ();

			Title = "Game of Life";
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			AutomaticallyAdjustsScrollViewInsets = true;

			InitToolbar ();

			InitActions ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			_gridView.Init (_grid, CellSize);
			LoadGridState (GliderStateKey);
		}

		private void InitActions ()
		{
			NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Action, HandleActions);
		}

		private void InitToolbar ()
		{
			_playButton = new UIBarButtonItem (UIBarButtonSystemItem.Play, HandleStart);
			_pauseButton = new UIBarButtonItem (UIBarButtonSystemItem.Pause, HandlePause);

			_pauseStateButtons = new[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				_playButton,
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace)
			};

			_playStateButtons = new[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				_pauseButton,
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace)
			};

			NavigationController.SetToolbarHidden (false, true);
			SetToolbarItems (_pauseStateButtons, true);
		}

		private void HandleStart(object sender, EventArgs args)
		{
			if (_timer == null) {
				_timer = NSTimer.CreateRepeatingScheduledTimer (GridViewUpdateInterval, Tick);
				NSRunLoop.Current.AddTimer (_timer, NSRunLoopMode.UITracking);

				SetToolbarItems (_playStateButtons, true);
			}
		}

		private void HandlePause(object sender, EventArgs args)
		{
			if (_timer != null) {
				_timer.Invalidate ();
				_timer = null;
				SetToolbarItems (_pauseStateButtons, true);
			}
		}

		private void HandleActions(object sender, EventArgs args)
		{
			HandlePause (this, null);

			if (_actionSheet == null) {
				_actionSheet = new UIActionSheet ("Actions", null, "Cancel", null, "Save grid", "Load saved grid", "Load gliders");
				_actionSheet.Clicked += HandleAction;
			}

			_actionSheet.ShowInView (View);
		}

		private void HandleAction (object sender, UIButtonEventArgs e)
		{
			switch (e.ButtonIndex) {
			case 0: // Save grid
				SaveGridState ();
				break;

			case 1: // Load saved grid
				LoadGridState (DefaultStateKey);
				break;

			case 2: // Load gliders
				LoadGridState (GliderStateKey);
				break;
			}
		}

		private void Tick()
		{
			var updatedCells = _grid.NextGeneration();

			if (updatedCells.Count == 0) {
				HandlePause (null, null);
			}

			_gridView.Update();
		}

		private void SaveGridState()
		{
			var serializedCoordinates = _serializer.Serialize (_grid.AliveCellsCoordinates);
			_stringsRepository.Save (DefaultStateKey, serializedCoordinates);
		}

		private void LoadGridState(string key)
		{
			HandlePause (null, null);

			var serializedCoordinates = _stringsRepository.GetByKey (key);

			if (!string.IsNullOrEmpty(serializedCoordinates)) {
				var coordinates = _serializer.Deserialize (serializedCoordinates);
				_grid.InitWith (coordinates);
			}

			_gridView.Reset ();
		}
	}
}

