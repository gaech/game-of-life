﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameOfLife.Core.Domain;

namespace GameOfLife.Core
{
	public class CellCoordinatesSerializer
	{
		public string Serialize(IEnumerable<CellCoordinates> coordinates)
		{
			if (coordinates == null) {
				throw new ArgumentNullException ("coordinates");
			}

			var sb = new StringBuilder ();

			var zeroX = coordinates.Min (c=>c.X);
			var zeroY = coordinates.Min (c=>c.Y);

			foreach (var c in coordinates) {
				sb.AppendFormat ("{0},{1}", c.X - zeroX, c.Y - zeroY);
				sb.Append (Environment.NewLine);
			}

			return sb.ToString ();
		}

		public IEnumerable<CellCoordinates> Deserialize(string data)
		{
			var result = new List<CellCoordinates> ();

			if (string.IsNullOrEmpty (data)) {
				throw new ArgumentNullException ("data");
			}

			string[] lines = data.Split (new []{ Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var line in lines) {
				string[] coordinates = line.Split (new []{ ',' }, StringSplitOptions.RemoveEmptyEntries);

				if (coordinates.Length != 2) {
					throw new Exception ("Invalid file format " + line);
				}

				result.Add(new CellCoordinates (UInt64.Parse(coordinates[0]), UInt64.Parse(coordinates[1])));
			}

			return result;
		}
	}
}

