﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace GameOfLife.Core.Domain
{
	public class Grid
	{
		private readonly object _sync = new object();

		private Dictionary<CellCoordinates,Cell> _aliveCells;

		public UInt64 Size {
			get;
			private set;
		}

		public IEnumerable<CellCoordinates> AliveCellsCoordinates {
			get {
				lock (_sync) {
					return new List<CellCoordinates> (_aliveCells.Keys);
				}
			}
		}

		public Grid () : this(UInt64.MaxValue)
		{
		}

		public Grid (UInt64 size)
		{
			_aliveCells = new Dictionary<CellCoordinates, Cell> ();
			Size = size;
		}

		public List<Cell> NextGeneration()
		{
			lock (_sync) {

				var allCells = GetAliveCellsAndNeighbours ();

				foreach (var cell in allCells.Values) {

					var aliveNeighboursCount = GetAliveNeighboursCount (cell.Coordinates);

					cell.IsAliveInFuture = (!cell.IsAliveNow && aliveNeighboursCount == 3) || // birth 
					(cell.IsAliveNow && aliveNeighboursCount >= 2 && aliveNeighboursCount <= 3);
				}


				var updatedCells = allCells.Values.Where (c => c.IsDirty).ToList ();

				UpdateAliveCells (updatedCells);


				return updatedCells;
			}
		}

		public void InitWith(IEnumerable<CellCoordinates> coordinates)
		{
			if (coordinates == null) {
				throw new ArgumentNullException ("coordinates");
			}

			lock (_sync) {

				_aliveCells.Clear ();

				AppendAliveCells (coordinates);
			}
		}

		public void ToggleCellState(UInt64 x, UInt64 y)
		{
			lock (_sync) {
				var coordinates = new CellCoordinates (x, y);

				if (_aliveCells.ContainsKey (coordinates)) {
					_aliveCells.Remove (coordinates);
				} else { // add alive cell
					var cell = new Cell (coordinates, true);
					_aliveCells.Add (coordinates, cell);
				}
			}
		}

		private Dictionary<CellCoordinates, Cell> GetAliveCellsAndNeighbours ()
		{
			// all alive cells
			var allCells = new Dictionary<CellCoordinates, Cell> (_aliveCells);
			// all dead neighbours
			foreach (var coordinates in _aliveCells.Keys) {
				var neighboursCoordinates = GetNeighboursCoordinates (coordinates).ToArray ();
				foreach (var neighbourCoordinates in neighboursCoordinates) {
					if (!allCells.ContainsKey (neighbourCoordinates)) {
						allCells.Add (neighbourCoordinates, new Cell (neighbourCoordinates, false));
					}
				}
			}
			return allCells;
		}

		private void UpdateAliveCells (List<Cell> updatedCells)
		{
			foreach (var cell in updatedCells) {
				if (cell.IsDeadman) {
					_aliveCells.Remove (cell.Coordinates);
				}
				if (cell.IsNewborn) {
					_aliveCells.Add (cell.Coordinates, cell);
				}
				cell.Reset ();
			}
		}

		private int GetAliveNeighboursCount(CellCoordinates c)
		{
			var aliveNeighboursCount = 0;

			foreach (var neighbourCoordinates in GetNeighboursCoordinates(c)) {
				if (_aliveCells.ContainsKey(neighbourCoordinates)) {
					aliveNeighboursCount++;
				}
			}

			return aliveNeighboursCount;
		}

		private void AppendAliveCells(IEnumerable<CellCoordinates> coordinates)
		{
			var width = coordinates.Max (c => c.Y) - coordinates.Min (c => c.Y);
			var height = coordinates.Max (c => c.X) - coordinates.Min (c => c.X);


			foreach (var c in coordinates) {

				var newCoordinates = new CellCoordinates (Size/2 - height/2 + c.X, Size/2 - width/2 + c.Y);

				if (!_aliveCells.ContainsKey(newCoordinates)) {
					_aliveCells.Add(newCoordinates, new Cell(newCoordinates, true));
				}

			}
		}

		private IEnumerable<CellCoordinates> GetNeighboursCoordinates(CellCoordinates c)
		{

			var result = new List<CellCoordinates> ();

			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {

					var skip = 
						(x == 0 && y == 0) ||
						(c.X == 0 && x < 0) ||
						(c.X == Size && x > 0) ||
						(c.Y == 0 && y < 0) ||
						(c.Y == Size && y > 0);

					if (skip) {
						continue;
					}

					UInt64 neighbourX = c.X;
					UInt64 neighbourY = c.Y;

					if (x < 0) {
						neighbourX -= 1;
					} else if (x > 0) {
						neighbourX += 1;
					}

					if (y < 0) {
						neighbourY -= 1;
					} else if (y > 0) {
						neighbourY += 1;
					}

					result.Add (new CellCoordinates (neighbourX, neighbourY));
				}
			}

			return result;
		}
	}
}

