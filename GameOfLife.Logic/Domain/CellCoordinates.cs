﻿using System;

namespace GameOfLife.Core.Domain
{
	public struct CellCoordinates
	{
		private readonly UInt64 _x;
		public UInt64 X {
			get {
				return _x;
			}
		}

		private readonly UInt64 _y;
		public UInt64 Y {
			get {
				return _y;
			}
		}

		public CellCoordinates (UInt64 x, UInt64 y)
		{
			_x = x;
			_y = y;
		}

		public override int GetHashCode ()
		{
			return X.GetHashCode () ^ Y.GetHashCode ();
		}

		public override bool Equals (object obj)
		{
			return obj is CellCoordinates && Equals ((CellCoordinates)obj);
		}

		private bool Equals(CellCoordinates other)
		{
			return X == other.X && Y == other.Y;
		}

		public override string ToString ()
		{
			return string.Format ("[CellCoordinates: X={0}, Y={1}]", X, Y);
		}
	}
}

