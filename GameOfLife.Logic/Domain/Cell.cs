﻿using System;

namespace GameOfLife.Core.Domain
{
	public class Cell
	{
		public CellCoordinates Coordinates {
			get;
			private set;
		}

		public bool IsAliveNow {
			get;
			set;
		}

		public bool IsAliveInFuture {
			get;
			set;
		}

		public bool IsNewborn {
			get {
				return !IsAliveNow && IsAliveInFuture;
			}
		}

		public bool IsDeadman {
			get {
				return IsAliveNow && !IsAliveInFuture;
			}
		}

		public bool IsDirty {
			get {
				return IsAliveNow != IsAliveInFuture;
			}
		}

		public Cell (UInt64 x, UInt64 y, bool isAlive) : this(new CellCoordinates(x, y), isAlive)
		{
		}

		public Cell (CellCoordinates coordinates, bool isAlive) 
		{
			Coordinates = coordinates;
			IsAliveNow = isAlive;
			IsAliveInFuture = isAlive;
		}

		public void Reset()
		{
			IsAliveNow = IsAliveInFuture;
		}
	}
}

