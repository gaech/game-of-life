﻿namespace GameOfLife.Core
{
	public interface IStringsRepository
	{
		string GetByKey(string key);
		void Save(string key, string value);
	}
}

