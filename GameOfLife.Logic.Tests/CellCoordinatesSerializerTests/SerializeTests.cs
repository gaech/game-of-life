﻿using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using System;

namespace GameOfLife.Core.Tests.CellCoordinatesSerializerTests
{
	[TestFixture]
	public class SerializeTests : TestBase
	{
		private CellCoordinatesSerializer _serializer;

		[SetUp]
		public void SetUp()
		{
			_serializer = new CellCoordinatesSerializer ();
		}

		[Test, ExpectedException(typeof(ArgumentNullException))]
		public void Should_throw_on_null ()
		{
			_serializer.Serialize (null);
		}

		[Test]
		public void Should_return_seriazlized_coordinates ()
		{
			// Arrange

			var coordinates = new [] {
				new CellCoordinates (10, 5),
				new CellCoordinates (39, 76),
				new CellCoordinates (11, 12)
			};

			// Act

			var serializedString = _serializer.Serialize (coordinates);

			// Assert

			var expectedString = "0,0\n29,71\n1,7\n";

			Assert.That (serializedString, Is.EqualTo (expectedString));
		}
	}
}

