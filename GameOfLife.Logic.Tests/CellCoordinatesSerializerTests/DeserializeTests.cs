﻿using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using System;

namespace GameOfLife.Core.Tests.CellCoordinatesSerializerTests
{
	[TestFixture]
	public class DeserializeTests : TestBase
	{
		private CellCoordinatesSerializer _serializer;

		[SetUp]
		public void SetUp()
		{
			_serializer = new CellCoordinatesSerializer ();
		}

		[Test, ExpectedException(typeof(ArgumentNullException))]
		public void Should_throw_on_null ()
		{
			var coordinates = _serializer.Deserialize (null);
		}

		[Test]
		public void Should_return_deseriazlized_coordinates ()
		{
			// Arrange

			var serializedString = "0,0\n29,71\n1,7\n";

			// Act

			var coordinates = _serializer.Deserialize (serializedString);

			// Assert

			var expectedCoordinates = new [] {
				new CellCoordinates (0, 0),
				new CellCoordinates (29, 71),
				new CellCoordinates (1, 7)
			};


			Assert.That (coordinates, Is.EquivalentTo(expectedCoordinates));
		}
	}
}

