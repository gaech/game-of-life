﻿using System;
using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using System.Linq;

namespace GameOfLife.Core.Tests.GridTests
{
	[TestFixture]
	public class StepTests : TestBase
	{
		[Test]
		public void Should_rotate_line ()
		{
			// Arrange 
			var input = new int[,]
			{
				{0, 0, 0},
				{1, 1, 1},
				{0, 0, 0}
			};

			var grid = CreateGrid (input);

			// Act
			var updatedCells = grid.NextGeneration ();

			// Assert
			var expectation = new int[,]
			{
				{0, 1, 0},
				{0, 1, 0},
				{0, 1, 0}
			};

			Assert.That (updatedCells, Is.Not.Null);
			Assert.That (updatedCells.Count, Is.EqualTo (4));

			AssertGrid (grid, expectation);
		}

		[Test]
		public void Should_return_empty ()
		{
			// Arrange 
			var input = new int[,]
			{
				{0, 0, 0},
				{0, 0, 0},
				{0, 0, 0}
			};

			var grid = CreateGrid (input);

			// Act
			var updatedCells = grid.NextGeneration ();

			// Assert

			Assert.That (updatedCells, Is.Empty);
		}

		private void AssertGrid(Grid grid, int[,] expectedStates)
		{
			for (int x = 0; x < expectedStates.GetLongLength(0); x++) {
				for (int y = 0; y < expectedStates.GetLength(1); y++) {

					if (expectedStates [x, y] == 1) { // isAlive
						var c = grid.AliveCellsCoordinates.First(cc=>cc.X == (UInt64)x && cc.Y == (UInt64)y);
						Assert.That (grid.AliveCellsCoordinates.Contains(c), Is.True);
					}
				}
			}
		}

		private Grid CreateGrid(int[,] states)
		{
			var grid = new Grid ();
			for (int x = 0; x < states.GetLongLength(0); x++) {
				for (int y = 0; y < states.GetLength(1); y++) {
					if (states [x, y] == 1) {
						grid.ToggleCellState ((UInt64)x, (UInt64)y);
					}
				}
			}

			return grid;
		}
	}
}

