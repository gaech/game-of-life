﻿using System;
using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using System.Linq;

namespace GameOfLife.Core.Tests.GridTests
{
	[TestFixture]
	public class InitWithTests : TestBase
	{
		[Test]
		public void Should_init_with_coordinates ()
		{
			// Arrange 
			var coordinates = new []
			{
				new CellCoordinates(0,0),
				new CellCoordinates(0,1),
				new CellCoordinates(2,4),
			};

			// Act

			var grid = new Grid ();
			grid.InitWith (coordinates);

			// Assert
			var expectedCoordinates = new []
			{
				new CellCoordinates(grid.Size/2 - 1, grid.Size/2 - 2),
				new CellCoordinates(grid.Size/2 - 1, grid.Size/2 - 1),
				new CellCoordinates(grid.Size/2 + 1, grid.Size/2 + 2),
			};

			Assert.That (grid.AliveCellsCoordinates.Count(), Is.EqualTo(coordinates.Length));
			Assert.That (grid.AliveCellsCoordinates, Is.EquivalentTo(expectedCoordinates));
		}
	}
}

