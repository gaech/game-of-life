﻿using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;

namespace GameOfLife.Core.Tests.CellCoordinatesTests
{
	[TestFixture]
	public class EqualsTests : TestBase
	{
		private CellCoordinates _coordinate;

		[SetUp]
		public override void Setup ()
		{
			base.Setup ();

			var x = _random.NextUInt64();
			var y = _random.NextUInt64();

			_coordinate = new CellCoordinates(x, y);
		}

		[Test]
		public void Should_return_false_on_null ()
		{
			Assert.That (_coordinate.Equals(null), Is.False);
		}

		[Test]
		public void Should_return_false_on_object ()
		{
			Assert.That (_coordinate.Equals(new object()), Is.False);
		}

		[Test]
		public void Should_return_false_on_diff_coordinates ()
		{
			// Arrange
			var b = new CellCoordinates(_random.NextUInt64(), _random.NextUInt64());

			// Act & Assert
			Assert.That (_coordinate.Equals(b), Is.False);
		}

		[Test]
		public void Should_return_true_on_same_coordinates ()
		{
			// Arrange
			var b = new CellCoordinates(_coordinate.X, _coordinate.Y);

			// Act & Assert
			Assert.That (_coordinate.Equals(b), Is.True);
		}
	}
}

