﻿using System;
using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;

namespace GameOfLife.Core.Tests.CellCoordinatesTests
{
	[TestFixture]
	public class GetHashCodeTests : TestBase
	{
		[Test]
		public void Should_return_x_XOR_y ()
		{

			// Arrange
			var x = _random.NextUInt64();
			var y = _random.NextUInt64();

			var a = new CellCoordinates(x, y);

			// Act
			var hashA = a.GetHashCode ();

			// Assert
			var expectedHash = x.GetHashCode () ^ y.GetHashCode ();
			Assert.That (hashA, Is.EqualTo (expectedHash));
		}

		[Test]
		public void Should_return_same_hash_for_same_coordinates ()
		{

			// Arrange
			var x = _random.NextUInt64();
			var y = _random.NextUInt64();

			var a = new CellCoordinates(x, y);
			var b = new CellCoordinates(x, y);

			// Act
			var hashA = a.GetHashCode ();
			var hashB = b.GetHashCode ();

			// Assert
			Assert.That (hashA, Is.EqualTo (hashB));
		}

		[Test]
		public void Should_return_diff_hash_for_diff_coordinates ()
		{

			// Arrange
			var a = new CellCoordinates(_random.NextUInt64(), _random.NextUInt64());
			var b = new CellCoordinates(_random.NextUInt64(), _random.NextUInt64());

			// Act
			var hashA = a.GetHashCode ();
			var hashB = b.GetHashCode ();

			// Assert
			Assert.That (hashA, Is.Not.EqualTo (hashB));
		}
	}
}

