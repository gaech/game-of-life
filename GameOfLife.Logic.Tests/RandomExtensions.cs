﻿using System;

namespace GameOfLife.Core.Tests
{
	public static class RandomExtensions
	{
		public static UInt64 NextUInt64(this Random random)
		{
			var buffer = new byte[sizeof(UInt64)];
			random.NextBytes(buffer);
			return BitConverter.ToUInt64(buffer, 0);
		}
	}
}

