﻿using System;
using GameOfLife.Core.Domain;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;

namespace GameOfLife.Core.Tests.CellTests
{
	[TestFixture]
	public class CtorTest : TestBase
	{
		[Test]
		public void Should_be_in_valid_state_after_ctor ()
		{
			var states = new []{ true, false };

			foreach (var state in states) {

				// Arrange
				var x = _random.NextUInt64 ();
				var y = _random.NextUInt64 ();

				//  Act
				var cell = new Cell (x, y, state);

				// Assert
				Assert.That (cell.IsAliveNow, Is.EqualTo (state));
				Assert.That (cell.IsAliveNow, Is.EqualTo (cell.IsAliveInFuture));
				Assert.That (cell.IsNewborn, Is.False);
				Assert.That (cell.IsDeadman, Is.False);
				Assert.That (cell.IsDirty, Is.False);
				Assert.That (cell.Coordinates.X, Is.EqualTo (x));
				Assert.That (cell.Coordinates.Y, Is.EqualTo (y));
			}
		}
	}
}

