﻿using System;
using NUnit.Framework;

namespace GameOfLife.Core.Tests
{
	public class TestBase
	{
		protected Random _random;

		[SetUp]
		public virtual void Setup ()
		{
			_random = new Random ();
		}
	}
}

